import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-project-small',
  templateUrl: './project-small.component.html',
  styleUrls: ['./project-small.component.scss']
})
export class ProjectSmallComponent implements OnInit {

  @Input() effectTitle = 'Small Project';
  @Input() githubLink = '#';
  @Input() youtubeLink = '#';
  @Input() videoTitle = 'Scalpel #99';
  @Input() time = '1h';
  @Input() skill = 'CSS';
  @Input() agency = 'Beniok';
  @Input() agencyWebsite = '#';
  @Input() website = '#';

  constructor() { }

  ngOnInit() {
  }

}
