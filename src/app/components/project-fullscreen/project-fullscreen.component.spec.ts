import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectFullscreenComponent } from './project-fullscreen.component';

describe('ProjectFullscreenComponent', () => {
  let component: ProjectFullscreenComponent;
  let fixture: ComponentFixture<ProjectFullscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectFullscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectFullscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
