import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-project-fullscreen',
  templateUrl: './project-fullscreen.component.html',
  styleUrls: ['./project-fullscreen.component.scss']
})
export class ProjectFullscreenComponent implements OnInit {

  @Input() effectTitle = 'Small Project';
  @Input() githubLink = '#';
  @Input() youtubeLink = '#';
  @Input() videoTitle = 'Scalpel #99';
  @Input() time = '1h';
  @Input() skill = 'CSS';
  @Input() agency = 'Beniok';
  @Input() agencyWebsite = '#';
  @Input() website = '#';

  constructor() { }

  ngOnInit() {
  }

}
