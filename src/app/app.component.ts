import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'scalpel-app';

  ngOnInit () {
    console.log(`
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░▄▄█▀▀███▀▀▀▀▀▄▄▄▄▄░░░░░░░░░░░
░░░░░░░░▄█████▀▀░▄▄▄░░░▄█░░░░▀█▄░░░░░░░░
░░░░░░▄█▄██▄▀░░░░░░▀▀▀▀▀░░░░░░░▀█▄░░░░░░
░░░░▄████▀░▀▀█░░░░░░░░░░▄█▀▀█░░░░█▄░░░░░
░░▄███▄▀▄█▀▀░░░░░░░░░░░▀▀░░░░░░░▄░█▄░░░░
░░▀██▀▄█▀░░░░░░░░░░░▄█░░░░░░░░░█▀░░█░░░░
░░██▀░░▀▀▄▄▄░░░░░░░█▀░░░░░░░░░░▀▀█▄█░░░░
░░██░░░░░▄█▀▀▀▀█▄▄▄█▄▄▄▄▄▄▄▄▄▄▄▄█▀▀█░░░░
░░██░░░░░█░░░▄▄░█▄░░░▄▀▀░░▀▀█▄░░░░░░█░░░
░░██░░░░░░▀▄▄▀▀▄██░░██░░░██░░█░░█░░░█░░░
░░██▄█▀░░░░░░▀▀░█░░▄█▀█▄▄▄▄▀▀░░░█░░░█░░░
░░█▀██▄░░░░░░░░░██▀▀░░░░░░░░░░░░▀█▄▄█░░░
░░▀████▄░░░░░▄▄█▀▀▀█▄▄░░▄█▀▀▀█▄░░░░█░░░░
░░░▀████▄░░░░█░░░░░░░░█░█▄░░░░░░░░█░░░░░
░░░░▀█▄███▄░░█░░░░░░░░█░░▀▄░░░░░▄█░░░░░░
░░░░░░▀████▄░▀░░░░░░░░▀░░░▀▀░░▄█▀░░░░░░░
░░░░░░░░▀████▄░░░░░░░░░░░░░░▄█▀░█▀▄░█▀█░
░░░░░░░░░░▀▀████▄▄▄▄▄▀█▄▄▄██▄░░░█░█░█▄█▄
    `);
    console.log("%cCAN I HELP YOU??? WHAT ARE YOU LOOKING FOR???", "background: black; color: white; font-size: normal");
    console.log('Just kidding my friend, you are right looking at the dev tools, it\'s the best way to learn!')
    console.log("%cSubscribe to my YouTube Channel", "background: black; color: white; font-size: small");
    console.log("%chttps://www.youtube.com/channel/UCLOAPb7ATQUs_nDs9ViLcMw?sub_confirmation=1", "background: transparent; color: white; font-size: normal");
  }
}
