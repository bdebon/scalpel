import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicAgencyComponent } from './basic-agency.component';

describe('BasicAgencyComponent', () => {
  let component: BasicAgencyComponent;
  let fixture: ComponentFixture<BasicAgencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicAgencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicAgencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
