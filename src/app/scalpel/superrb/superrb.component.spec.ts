import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperrbComponent } from './superrb.component';

describe('SuperrbComponent', () => {
  let component: SuperrbComponent;
  let fixture: ComponentFixture<SuperrbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperrbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperrbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
