import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UltranoirComponent } from './ultranoir.component';

describe('UltranoirComponent', () => {
  let component: UltranoirComponent;
  let fixture: ComponentFixture<UltranoirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UltranoirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UltranoirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
