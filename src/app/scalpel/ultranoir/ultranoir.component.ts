import {Component, ElementRef, OnInit} from '@angular/core';

@Component({
  selector: 'app-ultranoir',
  templateUrl: './ultranoir.component.html',
  styleUrls: ['./ultranoir.component.scss']
})
export class UltranoirComponent implements OnInit {

  cursor: any;
  title: any;

  constructor(private _elementRef: ElementRef) {
  }

  ngOnInit() {
    this.cursor = this._elementRef.nativeElement.getElementsByClassName('cursor')[0];

    this._elementRef.nativeElement.onmousemove = ev => {
      this.cursor.style.top = ev.clientY + 'px';
      this.cursor.style.left = ev.clientX + 'px';
    };

    this._elementRef.nativeElement.onmouseleave = ev => {
      this.cursor.style.display = 'none';
    };

    this._elementRef.nativeElement.onmouseenter = ev => {
      this.cursor.style.display = 'block';
    };

    this.title = this._elementRef.nativeElement.getElementsByTagName('h1')[0];

    this.title.onmouseover = ev => {
      this.cursor.classList.add('cursor--big');
    };

    this.title.onmouseleave = ev => {
      this.cursor.classList.remove('cursor--big');
    };

  }

}
