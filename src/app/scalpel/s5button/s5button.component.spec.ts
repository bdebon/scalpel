import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { S5buttonComponent } from './s5button.component';

describe('S5buttonComponent', () => {
  let component: S5buttonComponent;
  let fixture: ComponentFixture<S5buttonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ S5buttonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(S5buttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
